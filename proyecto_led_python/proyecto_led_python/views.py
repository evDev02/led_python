import pyrebase 
from django.shortcuts import render 

config = {
	"apiKey": "AIzaSyBrT8HT1DkrQ0FAhxXtEiyVRBdj972ljTg",
    "authDomain": "cir-led-38c98.firebaseapp.com",
    "databaseURL": "https://cir-led-38c98.firebaseio.com",
    "projectId": "cir-led-38c98",
    "storageBucket": "cir-led-38c98.appspot.com",
    "messagingSenderId": "19793709617",
    "appId": "1:19793709617:web:d9cded520c9fe120"
}
firebase = pyrebase.initialize_app(config)

db = firebase.database()


def InitPage(request):
	led = db.child('LED_STATUS').get().val()
	return render(request, "welcome.html",{"led":led})

def UpdateLedStatusOff(request):
	db.update({"LED_STATUS": "OFF"})
	led = db.child('LED_STATUS').get().val()
	return render(request, "welcome.html",{"led":led})

def UpdateLedStatusOn(request):
	db.update({"LED_STATUS": "ON"})
	led = db.child('LED_STATUS').get().val()
	return render(request, "welcome.html",{"led":led})