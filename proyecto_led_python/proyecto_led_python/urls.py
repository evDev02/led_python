from django.contrib import admin
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$',views.InitPage),
    url(r'Off',views.UpdateLedStatusOff, name='Off'),
    url(r'On',views.UpdateLedStatusOn, name='On'),
]
